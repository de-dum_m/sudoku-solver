/*
** get_grid.c for parse_input in /home/de-dum_m/B2-C-Prog-Elem/rush-1
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Fri Feb 28 21:57:47 2014 de-dum_m
** Last update Sat Mar  1 14:18:54 2014 de-dum_m
*/

#include <stdlib.h>
#include "get_next_line.h"
#include "my_printf.h"
#include "sudoku.h"

char	*get_grid_line(char *s)
{
  int	is;
  int	ig;
  char	*line;

  ig = 0;
  is = 2;
  if (!(line = malloc(10)) || (!s[0] || !s[1] || !s[2]))
    return (my_print_error_chret(PERRCOLNO));
  while (s[is] && ig < 9)
    {
      if (s[is] == ' ')
	line[ig++] = '0';
      else if (s[is] >= 48 && s[is] <= 57)
	line[ig++] = s[is];
      else
	return (my_print_error_chret(PERRBADGR));
      if (ig < 9 && (!s[++is] || !s[++is]))
	return (my_print_error_chret(PERRCOLNO));
    }
  line[ig] = '\0';
  return (line);
}

char	**get_grid(int jg)
{
  char	*s;
  char	**grid;

  if (!(grid = malloc(sizeof(char *) * 10)))
    return (NULL);
  free(get_next_line(0));
  while ((s = get_next_line(0)) && jg < 9)
    {
      if (!(grid[jg] = get_grid_line(s)) || my_strlen(grid[jg++]) != 9)
	return (NULL);
      free(s);
    }
  if (jg == 0)
    {
      free(grid);
      return (NULL);
    }
  else if (jg != 9)
    {
      free_tab(grid);
      return ((char **)my_print_error_chret(PERRLINENO));
    }
  grid[9] = NULL;
  free(s);
  return (grid);
}
