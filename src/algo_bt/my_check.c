/*
** my_check.c for src in /home/armita_a/Documents/Teck_1/Prog_elem/Sudoki-Bi/src
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Sat Mar  1 00:38:42 2014
** Last update Sun Mar  2 10:35:11 2014 de-dum_m
*/

#include "sudoku.h"

static int	my_check_col(t_sudo *ku, int nb)
{
  int		 cursor;

  cursor = 0;
  while (cursor <= 8)
    {
      if (nb + 48 == ku->sug[cursor][ku->count] && cursor != ku->index)
        return (FAILURE);
      cursor++;
    }
  return (SUCCESS);
}

static int	my_check_raw(t_sudo *ku, int nb)
{
  int		cursor;

  cursor = 0;
  while (cursor <= 8)
    {
      if (nb + 48 == ku->sug[ku->index][cursor] && cursor != ku->count)
        return (FAILURE);
      cursor++;
    }
  return (SUCCESS);
}

static int	my_check_square(t_sudo *ku, int nb)
{
  int		x;
  int		y;
  int		max_x;
  int		max_y;

  x = ku->count;
  y = ku->index;
  while (x % 3 != 0)
    x--;
  while (y % 3 != 0)
    y--;
  max_x = x + 2;
  max_y = y + 2;
  while (x <= max_x)
    {
      y = max_y - 2;
      while (y <= max_y)
        {
          if (nb + 48 == ku->sug[y][x] && (y != ku->index && x != ku->count))
	    return (FAILURE);
	  y++;
	}
      x++;
    }
  return (SUCCESS);
}

int	my_check(t_sudo *ku, int nb)
{
  if (nb <= 0 || nb > 9)
    return (FAILURE);
  if ((my_check_col(ku, nb) == SUCCESS)
      && (my_check_raw(ku, nb) == SUCCESS)
      && (my_check_square(ku, nb) == SUCCESS))
    return (SUCCESS);
  return (FAILURE);
}
