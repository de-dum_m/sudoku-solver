/*
** algo.c for algo_bt in /home/de-dum_m/B2-C-Prog-Elem/rush-1
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sat Mar  1 00:58:34 2014 de-dum_m
** Last update Sun Mar  2 15:53:50 2014 
*/

#include <stdlib.h>
#include <unistd.h>
#include "sudoku.h"

char	**my_tab_copy(char **grid)
{
  int	index;
  int	count;
  char	**tab;

  index = 0;
  if (!(tab = malloc(sizeof(char *) * 10)))
    return (NULL);
  while (index < 9)
    if (!(tab[index++] = malloc(sizeof(char) * 10)))
    return (NULL);
  index = 0;
  while (index < 9)
    {
      count = 0;
      while (count < 9)
	{
	  tab[index][count] = grid[index][count];
	  count++;
	}
      tab[index][count] = 0;
      index++;
    }
  tab[index] = NULL;
  return (tab);
}

static int	backtrack_mistake(t_sudo *ku)
{
  ku->sug[ku->index][ku->count] = '0';
  if (ku->count > 0)
    ku->count--;
  else if (ku->index > 0)
    {
      ku->index--;
      ku->count = 8;
    }
  while (ku->base[ku->index][ku->count] != '0')
    {
      if (ku->count > 0)
	ku->count--;
      else if (ku->index > 0)
	{
	  ku->index--;
	  ku->count = 8;
	}
    }
  ku->sug[ku->index][ku->count] += 1;
  return (SUCCESS);
}

static int	move_on(t_sudo *ku)
{
  if (ku->count < 8)
    ku->count++;
  else if (ku->index < 8)
    {
      ku->index++;
      ku->count = 0;
    }
  return (SUCCESS);
}

static void	print_verbose(t_sudo *ku)
{
  if (ku->verbose == SUCCESS)
    {
      my_print_sudoku(ku->sug, ku->print);
      usleep(ku->delay);
      my_put("cl");
    }
}

int	solve_it(t_sudo *ku)
{
  while (ku->index <= 8 && ku->count <= 8)
    {
      print_verbose(ku);
      if (ku->base[ku->index][ku->count] == '0')
	{
	  while (my_check(ku, ku->sug[ku->index][ku->count] - 48) == FAILURE
		 && ku->sug[ku->index][ku->count] < 57)
	    ku->sug[ku->index][ku->count] += 1;
	}
      if (ku->base[ku->index][ku->count] == '0'
	  && my_check(ku, ku->sug[ku->index][ku->count] - 48) == FAILURE)
	backtrack_mistake(ku);
      else if (ku->index == 8 && ku->count == 8)
	return (SUCCESS);
      else
	move_on(ku);
    }
  return (SUCCESS);
}
