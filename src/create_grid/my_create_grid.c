/*
** my_create_grid.c for create_grid in /home/armita_a/rendu/sudoki-bi/src/create_grid
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Sat Mar  1 17:55:08 2014
** Last update Sun Mar  2 14:39:12 2014 
*/

#include <term.h>
#include <unistd.h>
#include <stdlib.h>
#include <curses.h>
#include "create_grid.h"
#include "sudoku.h"
#include "my_printf.h"

static struct termios g_save;

void     my_escape()
{
  my_put("cl");
  my_put("vs");
  tcsetattr(0, 0, &g_save);
  my_putstr("Sorry to see you leave so soon.\n");
  exit(1);
}

static void	my_print_grid_back()
{
  my_put("cl");
  my_putstr("┏━━━┯━━━┯━━━┳━━━┯━━━┯━━━┳━━━┯━━━┯━━━┓\n");
  my_putstr("┃   │   │   ┃   │   │   ┃   │   │   ┃\n");
  my_putstr("┠───┼───┼───╂───┼───┼───╂───┼───┼───┨\n");
  my_putstr("┃   │   │   ┃   │   │   ┃   │   │   ┃\n");
  my_putstr("┠───┼───┼───╂───┼───┼───╂───┼───┼───┨\n");
  my_putstr("┃   │   │   ┃   │   │   ┃   │   │   ┃\n");
  my_putstr("┣━━━┿━━━┿━━━╋━━━┿━━━┿━━━╋━━━┿━━━┿━━━┫\n");
  my_putstr("┃   │   │   ┃   │   │   ┃   │   │   ┃\n");
  my_putstr("┠───┼───┼───╂───┼───┼───╂───┼───┼───┨\n");
  my_putstr("┃   │   │   ┃   │   │   ┃   │   │   ┃\n");
  my_putstr("┠───┼───┼───╂───┼───┼───╂───┼───┼───┨\n");
  my_putstr("┃   │   │   ┃   │   │   ┃   │   │   ┃\n");
  my_putstr("┣━━━┿━━━┿━━━╋━━━┿━━━┿━━━╋━━━┿━━━┿━━━┫\n");
  my_putstr("┃   │   │   ┃   │   │   ┃   │   │   ┃\n");
  my_putstr("┠───┼───┼───╂───┼───┼───╂───┼───┼───┨\n");
  my_putstr("┃   │   │   ┃   │   │   ┃   │   │   ┃\n");
  my_putstr("┠───┼───┼───╂───┼───┼───╂───┼───┼───┨\n");
  my_putstr("┃   │   │   ┃   │   │   ┃   │   │   ┃\n");
  my_putstr("┗━━━┷━━━┷━━━┻━━━┷━━━┷━━━┻━━━┷━━━┷━━━┛\n");
}

static int	raw_mode(struct termios *term)
{
  if (tcgetattr(0, term) == -1)
    return (-1);
  g_save = *term;
  term->c_lflag &= ~ICANON;
  term->c_lflag &= ~ECHO;
  term->c_cc[VMIN] = 1;
  term->c_cc[VTIME] = 0;
  tcsetattr(0, 0, term);
  if (tgetent(NULL, "xterm") <= 0)
    return (-1);
  return (0);
}

static char	**reset_grid(char **grid)
{
  int		index;
  int		count;

  index = 0;
  while (index <= 9)
    {
      count = 0;
      while (count <= 9)
	grid[index][count++] = '0';
      grid[index][count++] = 0;
      index++;
    }
  grid[index] = NULL;
  return (grid);
}

char			**my_create_grid()
{
  struct termios	term;
  char			**grid;
  int			index;

  index = 0;
  if (!(grid = malloc(sizeof(char *) * 10)))
    return (NULL);
  while (index <= 9)
    {
      if (!(grid[index++] = malloc(sizeof(char) * 10)))
	return (NULL);
    }
  raw_mode(&term);
  my_print_grid_back();
  my_print_rules();
  grid = reset_grid(grid);
  grid = my_fill_grid(grid);
  tcsetattr(0, 0, &g_save);
  return (grid);
}
