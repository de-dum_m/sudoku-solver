/*
** my_create_sudoku.c for create_grid in /home/armita_a/rendu/sudoki-bi
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Sat Mar  1 18:31:19 2014
** Last update Sun Mar  2 14:44:22 2014 
*/

#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include "create_grid.h"
#include "sudoku.h"

static void	my_loop(t_sel *pos)
{
  static int	val;
  char		c[4];
  int		nb;

  while (42)
    {
      signal(SIGINT, my_escape);
      nb = read(0, &c, 3);
      c[nb] = 0;
      if (c[0] == 27 && c[1] == 91 && c[2] == 66 && val == 0)
	move_down(pos);
      else if (c[0] == 27 && c[1] == 91 && c[2] == 65 &&  val == 0)
	move_up(pos);
      else if (c[0] == 27 && c[1] == 91 && c[2] == 67 && val == 0)
	move_right(pos);
      else if (c[0] == 27 && c[1] == 91 && c[2] == 68 && val == 0)
	move_left(pos);
      else if (c[0] >= '0' && c[0] <= '9' && !c[1])
	my_print_nb(pos, c[0], &val);
      else if (c[0] == 27 && !c[1])
	my_escape();
      else if (c[0] == '\n' && !c[1] && val == 0)
	return ;
    }
}

static void	go_to_base()
{
  int		index;

  index = 0;
  while (index++ != 18)
    my_put("up");
  my_put("nd");
  my_put("nd");
}

char	**my_fill_grid(char **grid)
{
  t_sel	pos;

  pos.grid = grid;
  pos.index = 0;
  pos.count = 0;
  go_to_base();
  my_loop(&pos);
  my_put("cl");
  return (pos.grid);
}

void	my_print_rules()
{
  int	index;

  index = 4;
  my_put("do");
  my_putstr("Use the arrow keys to move around\n");
  my_putstr("Use key pad to enter numbers\n");
  my_putstr("Use zero to erase and enter when you're finished\n");
  while (index-- != 0)
    my_put("up");
}
