/*
** my_check.c for create_grid in /home/armita_a/Documents/Teck_1/Prog_elem/rush-1/src/create_grid
** 
** Made by  armita_a
** Login   <armita_a@epitech.net>
** 
** Started on  Sun Mar  2 11:11:44 2014 
** Last update Sun Mar  2 12:06:33 2014 
*/

#include "create_grid.h"

static int	my_check_col(t_sel *pos, int nb)
{
  int		 cursor;

  cursor = 0;
  while (cursor <= 8)
    {
      if (nb + 48 == pos->grid[cursor][pos->count] && cursor != pos->index)
        return (FAILURE);
      cursor++;
    }
  return (SUCCESS);
}

static int	my_check_raw(t_sel *pos, int nb)
{
  int		cursor;

  cursor = 0;
  while (cursor <= 8)
    {
      if (nb + 48 == pos->grid[pos->index][cursor] && cursor != pos->count)
        return (FAILURE);
      cursor++;
    }
  return (SUCCESS);
}

static int	my_check_square(t_sel *p, int nb)
{
  int		x;
  int		y;
  int		max_x;
  int		max_y;

  x = p->count;
  y = p->index;
  while (x % 3 != 0)
    x--;
  while (y % 3 != 0)
    y--;
  max_x = x + 2;
  max_y = y + 2;
  while (x <= max_x)
    {
      y = max_y - 2;
      while (y <= max_y)
        {
          if (nb + 48 == p->grid[y][x] && (y != p->index && x != p->count))
	    return (FAILURE);
	  y++;
	}
      x++;
    }
  return (SUCCESS);
}

int	my_check_instant(t_sel *pos, int nb)
{
  if ((my_check_col(pos, nb) == SUCCESS)
      && (my_check_raw(pos, nb) == SUCCESS)
      && (my_check_square(pos, nb) == SUCCESS))
    return (SUCCESS);
  return (FAILURE);
}
