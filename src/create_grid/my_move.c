/*
** move.c for create_grid in /home/armita_a/rendu/sudoki-bi/src/create_grid
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Sat Mar  1 19:04:55 2014
** Last update Sun Mar  2 15:26:10 2014 
*/

#include "my_printf.h"
#include "create_grid.h"
#include "sudoku.h"

void	move_left(t_sel *pos)
{
  int	temp;

  pos->count--;
  temp = 0;
  while (temp++ != 2)
    my_put("le");
  if (pos->count == -1)
    while (pos->count < 8)
      move_right(pos);
  my_put("le");
  my_put("le");
}

void	move_right(t_sel *pos)
{
  int	temp;

  pos->count++;
  temp = 0;
  while (temp++ != 4)
    my_put("nd");
  if (pos->count == 9)
    while (pos->count > 0)
      move_left(pos);
}

void	move_up(t_sel *pos)
{
  pos->index--;
  my_put("up");
  if (pos->index == -1)
    while (pos->index < 8)
      move_down(pos);
  my_put("up");
}

void	move_down(t_sel *pos)
{
  int	temp;

 temp = pos->count;
 pos->count = 0;
 pos->index++;
 my_put("do");
 my_put("do");
 if (pos->index == 9)
   while (pos->index > 0)
     move_up(pos);
 my_put("nd");
 my_put("nd");
 while (pos->count < temp)
   move_right(pos);
}

void	my_print_nb(t_sel *pos, char c, int *val)
{
  my_put("vs");
  if (c == '0')
    {
      my_putchar(' ');
      *val = 0;
    }
  else
    {
      if (my_check_instant(pos, c - 48) == FAILURE)
	{
	  my_put("vi");
	  my_putstr(RED);
	  my_putchar(c);
	  *val = 1;
	}
      else
	{
	  my_putstr(GREEN);
	  my_putchar(c);
	  *val = 0;
	}
    }
  my_putstr(RES);
  pos->grid[pos->index][pos->count] = c;
  my_put("le");
}
