/*
** main.c for src in /home/de-dum_m/B2-C-Prog-Elem/rush-1
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Fri Feb 28 21:21:33 2014 de-dum_m
** Last update Sun Mar  2 16:19:23 2014 de-dum_m
*/

#include <stdlib.h>
#include "my_printf.h"
#include "sudoku.h"

void	free_tab(char **tab)
{
  int	i;

  i = 0;
  while (tab && tab[i])
    free(tab[i++]);
  free(tab);
}

static int	solve_sudoku(t_sudo *ku, int i)
{
  if (i++ != 0 && ku->print == FAILURE)
    my_putstr("####################\n");
  ku->base = my_tab_copy(ku->sug);
  ku->index = 0;
  ku->count = 0;
  if (solve_it(ku) == SUCCESS)
    my_print_sudoku(ku->sug, ku->print);
  else
    my_print_error(PERRBADGR);
  free_tab(ku->sug);
  free_tab(ku->base);
  return (SUCCESS);
}

int	print_usage()
{
  my_putstr("Usage: suduki-bi [OPTIONS] < [STDIN]\n");
  my_putstr("Options:\n");
  my_putstr("	-c	Create grid with gui.\n");
  my_putstr("	-h	Display this message.\n");
  my_putstr("	-p	Pretty print\n");
  my_putstr("	-v[=nb]	Slow down [step in microseconds (default: 50000)]\n");
  return (FAILURE);
}

int		main(int ac, char **av)
{
  t_sudo	ku;
  int		i;

  i = 0;
  if ((get_options(&ku, ac, av)) == FAILURE)
    return (SUCCESS);
  if (ku.cg_gui == SUCCESS)
    {
      ku.sug = my_create_grid();
      solve_sudoku(&ku, i);
    }
  else
    {
      while ((ku.sug = get_grid(0)))
	if (solve_sudoku(&ku, i++) != SUCCESS)
	  return (FAILURE);
    }
  return (SUCCESS);
}
