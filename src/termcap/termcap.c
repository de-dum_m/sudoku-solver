/*
** termcap.c for rush in /home/de-dum_m/B2-C-Prog-Elem/rush-1
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sat Mar  1 14:57:51 2014 de-dum_m
** Last update Sat Mar  1 15:18:51 2014 de-dum_m
*/

#include <unistd.h>
#include <curses.h>
#include <term.h>

static int	my_put_param(int c)
{
  write(1, &c, 1);
  return (1);
}

int	have_verbose()
{
  if (tgetent(NULL, "xterm") <= 0)
    return (-1);
  return (1);
}

int	my_put(char *cap)
{
  tputs(tgetstr(cap, NULL), 1, &my_put_param);
  return (1);
}
