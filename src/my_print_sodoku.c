/*
** my_print_sodoku.c for Sudoki-Bi in /home/armita_a/Documents/Teck_1/Prog_elem/Sudoki-Bi
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Fri Feb 28 21:26:44 2014
** Last update Sun Mar  2 15:31:28 2014 de-dum_m
*/

#include "my_printf.h"
#include "sudoku.h"

void	my_std_print(char **tab)
{
  int	index;
  int	count;

  index = 0;
  my_putstr("|------------------|\n");
  while (index <= 8)
    {
      my_putstr("| ");
      count = 0;
      while (count <= 8)
	{
	  my_putchar(tab[index][count]);
	  if (count != 8)
	    my_putchar(' ');
	  else
	    my_putstr("| \n");
	  count++;
	}
      index++;
    }
    my_putstr("|------------------|\n");
}

void	my_pretty_print(char **tab)
{
  int	index;
  int	count;

  index = 0;
  my_putstr("┏━━━┯━━━┯━━━┳━━━┯━━━┯━━━┳━━━┯━━━┯━━━┓\n");
  while (index <= 8)
    {
      count = 0;
      while (count <= 8)
	{
	  if (count % 3 == 0 && count != 0)
	    my_printf(" ┃ %c", tab[index][count++]);
	  else if (count == 8)
	    my_printf(" │ %c ┃", tab[index][count++]);
	  else if (count != 0)
	    my_printf(" │ %c", tab[index][count++]);
	  else
	    my_printf("┃ %c", tab[index][count++]);
	}
      if (++index % 3 == 0 && index != 0 && index != 9)
	my_putstr("\n┣━━━┿━━━┿━━━╋━━━┿━━━┿━━━╋━━━┿━━━┿━━━┫\n");
      else if (index != 0 && index != 9)
	my_printf("\n┠───┼───┼───╂───┼───┼───╂───┼───┼───┨\n");
    }
 my_putstr("\n┗━━━┷━━━┷━━━┻━━━┷━━━┷━━━┻━━━┷━━━┷━━━┛\n");
}

void	my_print_sudoku(char **tab, int nb)
{
  if (nb == FAILURE)
    my_std_print(tab);
  else
    my_pretty_print(tab);
}
