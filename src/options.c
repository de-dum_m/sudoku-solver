/*
** options.c for rush in /home/de-dum_m/B2-C-Prog-Elem/rush-1
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sun Mar  2 15:57:22 2014 de-dum_m
** Last update Sun Mar  2 16:24:15 2014 de-dum_m
*/

#include "sudoku.h"

static void	get_verbose_delay(t_sudo *ku, char *avi)
{
  ku->verbose = have_verbose();
  if (avi[2] && avi[2] == '=')
    ku->delay = my_getnbr(avi + 3);
  else
    ku->delay = 50000;
}

int	get_options(t_sudo *ku, int ac, char **av)
{
  int	i;

  i = 1;
  ku->verbose = FAILURE;
  ku->cg_gui = FAILURE;
  ku->print = FAILURE;
  while (i < ac)
    {
      if (my_strcmp(av[i], "-c") == 0)
	ku->cg_gui = SUCCESS;
      else if (av[i][0] == '-' && av[i][1] && av[i][1] == 'v')
	get_verbose_delay(ku, av[i]);
      else if (my_strcmp(av[i], "-p") == 0)
	ku->print = SUCCESS;
      else if (my_strcmp(av[i], "-h") == 0)
	return (print_usage());
      i++;
    }
  return (SUCCESS);
}
