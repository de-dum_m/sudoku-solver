/*
** mamelocs.c for rush in /home/de-dum_m/B2-C-Prog-Elem/rush-1
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sat Mar  1 00:47:21 2014 de-dum_m
** Last update Sat Mar  1 08:46:26 2014 
*/

#include <stdlib.h>
#include "sudoku.h"

char	*my_str_copy(char *s, int len)
{
  int	i;
  char	*res;

  i = 0;
  if (len == 0)
    len = my_strlen(s);
  else if (len < 0)
    len = my_strlen(s) - len;
  if (!(res = malloc(len + 3)))
    return (NULL);
  while (s && s[i] && i < len)
    {
      res[i] = s[i];
      i++;
    }
  res[i] = '\0';
  return (res);
}
