/*
** my_strcmp.c for rush in /home/de-dum_m/B2-C-Prog-Elem/rush-1
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sat Mar  1 15:00:06 2014 de-dum_m
** Last update Sat Mar  1 15:00:06 2014 de-dum_m
*/

int	my_strcmp(char *s1, char *s2)
{
  int	 i;

  i = 0;
  while ((s1 && s2) && (s1[i] == s2[i] && s1[i] != '\0' && s2[i] != '\0'))
    i = i + 1;
  if (s1 && s2 && s1[i] == s2[i])
    return (0);
  if (s1 && s2 && (s1[i] < s2[i] || s1[i] == '\0'))
    return (-1);
  return (1);
}
