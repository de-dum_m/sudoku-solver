/*
** my_count_putchar.c for rush in /home/de-dum_m/B2-C-Prog-Elem/rush-1
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sun Mar  2 16:07:12 2014 de-dum_m
** Last update Sun Mar  2 16:07:12 2014 de-dum_m
*/

#include <unistd.h>

int	count_prints()
{
  static int	prints;

  prints = prints + 1;
  return (prints);
}

void	my_count_putchar(char c, int fd)
{
  char	space;

  space = ' ';
  if (!c)
    return ;
  if (c == '0')
    write(fd, &space, 1);
  else
    write(fd, &c, 1);
  count_prints();
}

void	my_count_putstr(char *str, int fd)
{
  int	i;

  i = 0;
  while (str && str[i])
    {
      my_count_putchar(str[i], fd);
      i = i + 1;
    }
}
