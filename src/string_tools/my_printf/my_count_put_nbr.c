/*
** my_count_put_nbr.c for rush in /home/de-dum_m/B2-C-Prog-Elem/rush-1
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sun Mar  2 16:07:51 2014 de-dum_m
** Last update Sun Mar  2 16:07:51 2014 de-dum_m
*/

#include "my_printf.h"

int	my_count_put_nbr(int nb)
{
  if (nb < 0)
    {
      nb = - nb;
      my_count_putchar('-', 1);
    }
  if (nb >= 10)
    {
      my_count_put_nbr(nb / 10);
      my_count_put_nbr(nb % 10);
    }
  else
    print_modified_count(nb);
  return (0);
}
