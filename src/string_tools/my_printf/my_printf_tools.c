/*
** my_printf_tools.c for lib in /home/de-dum_m/code/B1-Systeme_Unix/minishell2/src/lib
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Thu Feb 20 18:02:54 2014 de-dum_m
** Last update Fri Feb 28 21:45:52 2014 de-dum_m
*/

#include <stdarg.h>
#include "my_printf.h"

void	my_put_hex(unsigned int ix, char type)
{
  char		*base;

  base = "0123456789abcdef";
  if (type == 'X')
    base = "0123456789ABCDEF";
  my_count_putnbr_base(ix, base);
}

void	my_putstr_weird(char *str, int fd)
{
  int		i;

  i = 0;
  while (str[i])
    {
      if (str[i] >= 32 && str[i] < 127)
	my_count_putchar(str[i], fd);
      else
	{
	  my_count_putchar('\\', fd);
	  if (str[i] <= 7)
	    my_count_putchar('0', fd);
	  if (str[i] <= 63)
	    my_count_putchar('0', fd);
	  my_count_putnbr_base(str[i], "01234567");
	}
      i = i + 1;
    }
}

void	my_put_ptr(unsigned int ptr)
{
  if (ptr > 0)
    my_count_putstr("0x", 1);
  if (ptr > 16777215)
    my_count_putstr("7fff", 1);
  if (ptr == 0)
    my_count_putstr("(nil)", 1);
  else if (ptr < 16777215 && ptr > 16777215)
    my_count_putstr("00", 1);
  else if (ptr < 268435455 && ptr > 16777215)
    my_count_putchar('0', 1);
  if (ptr > 0)
    my_count_putnbr_base(ptr, "0123456789abcdef");
}
