/*
** my_puts.c for rush in /home/de-dum_m/B2-C-Prog-Elem/rush-1
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sun Mar  2 16:04:56 2014 de-dum_m
** Last update Sun Mar  2 16:04:57 2014 de-dum_m
*/

#include <unistd.h>

int	my_strlen(char *str)
{
  int	index;

  index = 0;
  while (str[index])
    index++;
  return (index);
}

void	my_putchar(char c)
{
  write(1, &c, 1);
}

void	my_putstr(char *str)
{
  write(1, str, my_strlen(str));
}
