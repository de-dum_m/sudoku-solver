/*
** new_get_nbr.c for my_lib in /home/de-dum_m/code/B1-C-Prog_Elem/CPE_2013_Pushswap
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Thu Dec 26 10:59:18 2013 de-dum_m
** Last update Thu Dec 26 12:07:41 2013 de-dum_m
*/

int	my_getnbr(char *str)
{
  int	i;
  int	res;

  i = 0;
  res = 0;
  while (str[i])
    {
      if (str[i] >= '0' && str[i] <= '9')
	res = (res * 10) + (str[i] - 48);
      else if (str[i] != '-')
	return (res);
      i++;
    }
  if (str[0] == '-')
    res = - res;
  return (res);
}
