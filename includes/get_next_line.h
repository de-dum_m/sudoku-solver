/*
** get_next_line.h for includes in /home/armita_a/rendu/sudoki-bi/includes
** 
** Made by  armita_a
** Login   <armita_a@epitech.net>
** 
** Started on  Sun Mar  2 12:29:03 2014 
** Last update Sun Mar  2 12:29:48 2014 
*/

#ifndef GET_NEXT_LINE_H_
# define GET_NEXT_LINE_H_

# define BUFSIZ		8

# define SUCCESS	1
# define FAILURE	-1

char	*get_next_line(int fd);

#endif /* !GET_NEXT_LINE_H_ */
