/*
** create_grid.h for includes in /home/armita_a/rendu/sudoki-bi/includes
** 
** Made by  armita_a
** Login   <armita_a@epitech.net>
** 
** Started on  Sun Mar  2 12:28:49 2014 
** Last update Sun Mar  2 14:39:57 2014 
*/

#ifndef CREATE_GRID_H_
# define CREATE_GRID_H_

# define GREEN		"\033[032m"
# define RED		"\033[1;031m"
# define RES		"\033[0m"

# define SUCCESS	1
# define FAILURE	-1

typedef struct	s_sel
{
  int		index;
  int		count;
  char		**grid;
}		t_sel;

/*
** my_create_sudoku.c
*/
char	**my_fill_grid(char **grid);
void	my_print_rules();

/*
** my_move.c
*/
void	move_down(t_sel *pos);
void	move_left(t_sel *pos);
void	move_right(t_sel *pos);
void	move_up(t_sel *pos);
void	my_print_nb(t_sel *pos, char c, int *val);

/*
** my_check.c
*/
int	my_check_instant(t_sel *pos, int nb);

/*
** my_create_grid.c
*/
void	my_escape();

#endif /* !CREATE_GRID_H_ */
