/*
** sodoku.h for includes in /home/armita_a/Documents/Teck_1/Prog_elem/Sudoki-Bi/includes
**
** Made by  armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Fri Feb 28 21:47:25 2014
** Last update Sun Mar  2 16:20:20 2014 de-dum_m
*/

#ifndef SUDOKU_H_
# define SUDOKU_H_

# define FAILURE	-1
# define SUCCESS	1

# define PERRLINENO	"Not enough lines\n"
# define PERRCOLNO	"Not enought colums\n"
# define PERRBADGR	"Bad grid\n"

typedef struct	s_sudo
{
  char		**sug;
  char		**base;
  int		index;
  int		count;
  int		verbose;
  int		cg_gui;
  int		print;
  int		delay;
}		t_sudo;

/*
** main.c
*/
int	print_usage();
void	free_tab(char **my_tab);

/*
** my_print_sodoku.c
*/
void	my_pretty_print(char **my_tab);
void	my_print_sudoku(char **my_tab, int nb);
void	my_std_print(char **my_tab);

/*
** options.c
*/
int	get_options(t_sudo *ku, int ac, char **av);

/*
** algo.c
*/
char	**my_tab_copy(char **grid);
int	solve_it(t_sudo *ku);

/*
** my_check.c
*/
int	my_check(t_sudo *ku, int nb);

/*
** get_grid.c
*/
char	**get_grid(int jg);
char	*get_grid_line(char *s);

/*
** get_next_line.c
*/
char	*get_next_line(int fd);

/*
** mamelocs.c
*/
char	*my_str_copy(char *s, int len);

/*
** my_getnbr.c
*/
int	my_getnbr(char *str);

/*
** my_puts.c
*/
int	my_strlen(char *str);
void	my_putchar(char c);
void	my_putstr(char *str);

/*
** my_strcmp.c
*/
int	my_strcmp(char *s1, char *s2);

/*
** termcap.c
*/
int	have_verbose();
int	my_put(char *cap);

/*
** create_grid.c
*/
char	**my_create_grid();

#endif /* !SUDOKU_H_ */
