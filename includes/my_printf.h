/*
** my_printf.h for includes in /home/armita_a/rendu/sudoki-bi/includes
** 
** Made by  armita_a
** Login   <armita_a@epitech.net>
** 
** Started on  Sun Mar  2 12:30:25 2014 
** Last update Sun Mar  2 12:30:31 2014 
*/

#ifndef MY_PRINTF_H_
# define MY_PRINTF_H_

/*
** my_count_putchar.c
*/
int	count_prints();
void	my_count_putchar(char c, int fd);
void	my_count_putstr(char *str, int fd);

/*
** my_count_put_nbr_base.c
*/
int	my_count_putnbr_base(unsigned int nbr, char *base);
void	chop_chop_count(unsigned int nbr, int b, char *base);

/*
** my_count_put_nbr.c
*/
int	my_count_put_nbr(int nb);

/*
** my_printf.c
*/
int	my_print_error(const char *str, ...);
int	my_printf(const char *str, ...);
char	*my_print_error_chret(const char *str, ...);
/*
** my_printf_tools.c
*/
void	my_put_hex(unsigned int ix, char type);
void	my_put_ptr(unsigned int ptr);
void	my_putstr_weird(char *str, int fd);

/*
** my_put_unsigned_nbr.c
*/
int	my_put_unsigned_nbr(unsigned int nb);
void	print_modified_count(char nb);

#endif /* !MY_PRINTF_H_ */
