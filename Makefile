##
## Makefile for rush-1 in /home/de-dum_m/B2-C-Prog-Elem/rush-1
## 
## Made by de-dum_m
## Login   <de-dum_m@epitech.net>
## 
## Started on  Fri Feb 28 21:18:53 2014 de-dum_m
## Last update Sun Mar  2 16:14:30 2014 de-dum_m
##

BINDIR	= bin/

NAME	= bin/sudoki-bi

SRC	= src/algo_bt/algo.c \
	src/algo_bt/my_check.c \
	src/create_grid/my_check.c \
	src/create_grid/my_create_grid.c \
	src/create_grid/my_create_sudoku.c \
	src/create_grid/my_move.c \
	src/main.c \
	src/my_print_sodoku.c \
	src/options.c \
	src/parse_input/get_grid.c \
	src/string_tools/get_next_line.c \
	src/string_tools/mamelocs.c \
	src/string_tools/my_getnbr.c \
	src/string_tools/my_printf/my_count_putchar.c \
	src/string_tools/my_printf/my_count_put_nbr_base.c \
	src/string_tools/my_printf/my_count_put_nbr.c \
	src/string_tools/my_printf/my_printf.c \
	src/string_tools/my_printf/my_printf_tools.c \
	src/string_tools/my_printf/my_put_unsigned_nbr.c \
	src/string_tools/my_puts.c \
	src/string_tools/my_strcmp.c \
	src/termcap/termcap.c

OBJ	= $(SRC:.c=.o)

CFLAGS	= -Wall -Wextra -W -pedantic -Iincludes

$(NAME): $(OBJ)
	test -d $(BINDIR) || mkdir $(BINDIR)
	$(CC) $(OBJ) -o $(NAME) -lncurses
	@echo -e "[032mCompiled successfully[0m"

all:	$(NAME)

clean:
	rm -f $(OBJ)

fclean:	clean
	rm -f $(NAME)

re:	fclean all

.PHONY:	all re fclean clean
